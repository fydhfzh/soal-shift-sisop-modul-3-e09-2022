#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

pid_t child;
pthread_t tid1[2], tid2[2], tid3[2];

void* moveFileThread(void *arg)
{
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	
	if(pthread_equal(id,tid3[0])) // thread move music
	{
        child = fork();
        if (child==0) {
        	char *argv[] = {"mv","/home/gery/music/music.txt", "/home/gery/hasil", NULL};
        	execv("/bin/mv", argv);
		
	    }
	}
	
	else if(pthread_equal(id,tid3[1])) // thread decode quote
	{
        child = fork();
        if (child==0) {
		char *argv[] = {"mv","/home/gery/quote/quote.txt", "/home/gery/hasil", NULL};
        	execv("/bin/mv", argv);	
	    }
	}
}

void moveFile()
{
	int i=0;
	int err;
	while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid3[i]),NULL,&moveFileThread,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread MOVE : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success MOVE\n");
		}
		i++;
	}
	pthread_join(tid3[0],NULL);
	pthread_join(tid3[1],NULL);
	exit(0);
}

void moveFile2()
{
	pid_t child;
	int status;
	child = fork();
	if (child==0) {
		char *argv[] = {"mv","/home/gery/music/music.txt", "/home/gery/hasil", NULL};
        	execv("/bin/mv", argv);
	}
	else
	{
		while((wait(&status)) > 0);
		char *argv[] = {"mv","/home/gery/quote/quote.txt", "/home/gery/hasil", NULL};
        	execv("/bin/mv", argv);
	}
}

void makeFolder()
{
	pid_t child;
	int status;
	child = fork();
	if (child==0) {
		char *argv[] = {"mkdir", "-p", "/home/gery/hasil", NULL};
        	execv("/bin/mkdir", argv);
	}
	else
	{
		while((wait(&status)) > 0);
		moveFile2();
	}
}

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
}

void readTextMusic()
{
	DIR *dp;
    	struct dirent *ep;
	FILE *fPtrW, *fPtrR;
	
    	char pathAwalMusic[]="/home/gery/music/";
    	char pathSrcMusic[]="/home/gery/music/";
    	char hasil[500];
	dp = opendir(pathAwalMusic);  
	
	fPtrW = fopen("/home/gery/music/music.txt", "w");
    	if(fPtrW == NULL)
    	{
        	printf("Unable to create file.\n");
        	exit(EXIT_FAILURE);
    	} 
	if (dp != NULL)
	{
     		while ((ep = readdir (dp))) {
     			if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          		strcpy(pathSrcMusic, "/home/gery/music/");
          		strcat(pathSrcMusic, ep->d_name);
        	  		
          		fPtrR = fopen(pathSrcMusic, "r");
    			if(fPtrR == NULL)
    			{
        			printf("Unable to read file. %s \n", pathSrcMusic);
        			exit(EXIT_FAILURE);
    			}
    			
    			
    			char line[256];
    			while(fgets(line, sizeof(line), fPtrR))
    			{
    				long decode_size = strlen(line);
    				char * decoded_data = base64_decode(line, decode_size, &decode_size);
    				strcat(hasil, decoded_data);
    			}
    			
    			strcat(hasil, "\n");
          		
          		fclose(fPtrR);	
        	}
        	(void) closedir (dp);
        	
        	fprintf(fPtrW,"%s\n", hasil);
        	fclose(fPtrW);
        	
        } else perror ("Couldn't open the directory");
}

void readTextQuote()
{
	DIR *dp;
    	struct dirent *ep;
	FILE *fPtrW, *fPtrR;
	
    	char pathAwalQuote[]="/home/gery/quote/";
    	char pathSrcQuote[]="/home/gery/quote/";
    	char hasil[500];
	dp = opendir(pathAwalQuote);  
	
	fPtrW = fopen("/home/gery/quote/quote.txt", "w");
    	if(fPtrW == NULL)
    	{
        	printf("Unable to create file.\n");
        	exit(EXIT_FAILURE);
    	} 
	if (dp != NULL)
	{
     		while ((ep = readdir (dp))) {
     			if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          		strcpy(pathSrcQuote, "/home/gery/quote/");
          		strcat(pathSrcQuote, ep->d_name);
        	  		
          		fPtrR = fopen(pathSrcQuote, "r");
    			if(fPtrR == NULL)
    			{
        			printf("Unable to read file. %s \n", pathSrcQuote);
        			exit(EXIT_FAILURE);
    			}
    			
    			
    			char line[256];
    			while(fgets(line, sizeof(line), fPtrR))
    			{
    				long decode_size = strlen(line);
    				char * decoded_data = base64_decode(line, decode_size, &decode_size);
    				strcat(hasil, decoded_data);
    			}
    			
    			strcat(hasil, "\n");
          		
          		fclose(fPtrR);	
        	}
        	(void) closedir (dp);
        	
        	fprintf(fPtrW,"%s\n", hasil);
        	fclose(fPtrW);
        	
        } else perror ("Couldn't open the directory");
}

void* decodeThread(void *arg)
{
	
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	int status;
	
	if(pthread_equal(id,tid2[0])) // thread decode music
	{
        child = fork();
        if (child==0) {
        	printf("\n exe argv1\n");
		//execv("/bin/unzip", argv1);
		readTextMusic();
		
	    }
	}
	
	else if(pthread_equal(id,tid2[1])) // thread decode quote
	{
        child = fork();
        if (child==0) {
		printf("\n exe argv2\n");
		readTextQuote();		
	    }
	else
	{
		while((wait(&status)) > 0);
		makeFolder();
	}
	}
}

int length=5; //inisialisasi jumlah untuk looping
void* unzipThread(void *arg)
{
	int status;
	char *argv1[] = {"unzip", "music.zip", "-d", "music", NULL};
	char *argv2[] = {"unzip", "quote.zip", "-d", "quote", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	
	if(pthread_equal(id,tid1[0])) // thread unzip music
	{
        child = fork();
        if (child==0) {
		    execv("/bin/unzip", argv1);
	    }
	}
	else if(pthread_equal(id,tid1[1])) // thread unzip quote
	{
        child = fork();
        if (child==0) {
		    execv("/bin/unzip", argv2);
	    }
	
	}
	

	return NULL;
}

void unzipFile()
{
	int i=0;
	int err;
	while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid1[i]),NULL,&unzipThread,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread UNZIP : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success UNZIP\n");
		}
		i++;
	}
	pthread_join(tid1[0],NULL);
	pthread_join(tid1[1],NULL);
	exit(0);
}


void decodeFile()
{
	int i=0;
	int err;
	while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid2[i]),NULL,&decodeThread,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread DECODE : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success DECODE\n");
		}
		i++;
	}
	pthread_join(tid2[0],NULL);
	pthread_join(tid2[1],NULL);
	
}

int main(void)
{
	pid_t child;
	int status;
	child = fork();
	if (child==0) {
		unzipFile();
	}
	else
	{
		while((wait(&status)) > 0);
		decodeFile();
	}
	return 0;
}


