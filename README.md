## Nama Anggota:
### -Mohammad Firman Fardiansyah 5025201109
### -Gery Febrian Setyara 5025201151
### -Fayyadh Hafizh 5025201164

## Soal 1
Pada soal 1, kita diminta untuk memecahkan kode dengan cara decoding menggunakan base64

### Poin A: Download dan Unzip File
Untuk menyelesaikan ini kita harus mendownload dulu filenya dari link google drive yang telah diberikan dan mengunzip filenya ke directory masing masing
    
    void* unzipThread(void *arg)
    {
	int status;
	char *argv1[] = {"unzip", "music.zip", "-d", "music", NULL};
	char *argv2[] = {"unzip", "quote.zip", "-d", "quote", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	
	if(pthread_equal(id,tid1[0])) // thread unzip music
	{
    child = fork();
    if (child==0) {
		    execv("/bin/unzip", argv1);
    }
	}
	else if(pthread_equal(id,tid1[1])) // thread unzip quote
	{
    child = fork();
    if (child==0) {
		    execv("/bin/unzip", argv2);
	}
	
	}
	

	return NULL;
    }

    void unzipFile()
    {
	int i=0;
	int err;
	while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid1[i]),NULL,&unzipThread,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread UNZIP : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success UNZIP\n");
		}
		i++;
	}
	pthread_join(tid1[0],NULL);
	pthread_join(tid1[1],NULL);
	exit(0);
    }


### Poin B: Decode file Music dan Quote 
Disini diminta untuk mendecode file music dan quote menggunakan base64 dan masing-masing hasil akan di berikan newline

    
    static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','I', 'J', 'K', 'L', 'M', 'N', 'O', 'P','Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X','Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f','g', 'h', 'i', 'j', 'k','l', 'm', 'n','o', 'p', 'q', 'r', 's', 't', 'u', 'v','w', 'x', 'y', 'z', '0', '1', '2', '3','4', '5', '6', '7', '8', '9', '+', '/'};
    static char *decoding_table = NULL;
    static int mod_table[] = {0, 2, 1};
 
    void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
    decoding_table[(unsigned char) encoding_table[i]] = i;
    }

    unsigned char *base64_decode(const char *data,
    size_t input_length,
    size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
    uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
    uint32_t triple = (sextet_a << 3 * 6)
    +(sextet_b << 2 * 6)
    +(sextet_c << 1 * 6)
    +(sextet_d << 0 * 6);
 
    if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
    if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
    if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
    }

### Setelah selesai di inilisiasi maka format .txt dari music dan quote di read dan di decode menggunakan fungsi diatas

    void readTextMusic()
    {
	DIR *dp;
    	struct dirent *ep;
	FILE *fPtrW, *fPtrR;
	
    	char pathAwalMusic[]="/home/gery/music/";
    	char pathSrcMusic[]="/home/gery/music/";
    	char hasil[500];
	dp = opendir(pathAwalMusic);  
	
	fPtrW = fopen("/home/gery/music/music.txt", "w");
    	if(fPtrW == NULL)
    	{
    Printf("Unable to create file.\n");
    exit(EXIT_FAILURE);
    	} 
	if (dp != NULL)
	{
    While ((ep = readdir (dp))) {
    if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
    strcpy(pathSrcMusic, "/home/gery/music/");
    strcat(pathSrcMusic, ep->d_name);
        	  		
    fPtrR = fopen(pathSrcMusic, "r");
    if(fPtrR == NULL)
    {
    printf("Unable to read file. %s \n", pathSrcMusic);
    exit(EXIT_FAILURE);
    }
    			
    			
    			char line[256];
    			while(fgets(line, sizeof(line), fPtrR))
    			{
    				long decode_size = strlen(line);
    				char * decoded_data = base64_decode(line, decode_size, &decode_size);
    				strcat(hasil, decoded_data);
    			}
    			
    			strcat(hasil, "\n");
          		
    fclose(fPtrR);	
    	   }
    (void) closedir (dp);
        	
    fprintf(fPtrW,"%s\n", hasil);
    fclose(fPtrW);
        	
    } else perror ("Couldn't open the directory");
    }

    void readTextQuote()
    {
	DIR *dp;
    	struct dirent *ep;
	FILE *fPtrW, *fPtrR;
	
    	char pathAwalQuote[]="/home/gery/quote/";
    	char pathSrcQuote[]="/home/gery/quote/";
    	char hasil[500];
	dp = opendir(pathAwalQuote);  
	
	fPtrW = fopen("/home/gery/quote/quote.txt", "w");
    	if(fPtrW == NULL)
    	{
    printf("Unable to create file.\n");
    exit(EXIT_FAILURE);
    	} 
	if (dp != NULL)
	{while ((ep = readdir (dp))) {
    if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
    strcpy(pathSrcQuote, "/home/gery/quote/");
    strcat(pathSrcQuote, ep->d_name);
        	  		
    fPtrR = fopen(pathSrcQuote, "r");
    			if(fPtrR == NULL)
    			{
    printf("Unable to read file. %s \n", pathSrcQuote);
    exit(EXIT_FAILURE);
    			}
    			
    			
    			char line[256];
    			while(fgets(line, sizeof(line), fPtrR))
    			{
    				long decode_size = strlen(line);
    				char * decoded_data = base64_decode(line, decode_size, &decode_size);
    				strcat(hasil, decoded_data);
    			}
    			
    			strcat(hasil, "\n");
          		
    fclose(fPtrR);	
    }
    	(void) closedir (dp);
        	
    	fprintf(fPtrW,"%s\n", hasil);
    	fclose(fPtrW);
        	
    } else perror ("Couldn't open the directory");
    }

### Poin C: Move File Music dan Quote ke Hasil
Memindahkan file music dan quote yang telah terdecode ke directory hasil

    void* moveFileThread(void *arg)
    {
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	
	if(pthread_equal(id,tid3[0])) // thread move music
	{
    child = fork();
    if (child==0) {
    	char *argv[] = {"mv","/home/gery/music/music.txt", "/home/gery/hasil", NULL};
    	execv("/bin/mv", argv);
		
	}
	}
	
	else if(pthread_equal(id,tid3[1])) // thread decode quote
	{
    child = fork();
    if (child==0) {
		char *argv[] = {"mv","/home/gery/quote/quote.txt", "/home/gery/hasil", NULL};
    	execv("/bin/mv", argv);	
	}
	}
    }

    void moveFile()
    {
	int i=0;
	int err;
	while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid3[i]),NULL,&moveFileThread,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread MOVE : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success MOVE\n");
		}
		i++;
	}
	pthread_join(tid3[0],NULL);
	pthread_join(tid3[1],NULL);
	exit(0);
    }

    void moveFile2()
    {
	pid_t child;
	int status;
	child = fork();
	if (child==0) {
		char *argv[] = {"mv","/home/gery/music/music.txt", "/home/gery/hasil", NULL};
    	execv("/bin/mv", argv);
	}
	else
	{
		while((wait(&status)) > 0);
		char *argv[] = {"mv","/home/gery/quote/quote.txt", "/home/gery/hasil", NULL};
    	execv("/bin/mv", argv);
	}
    }

    void makeFolder()
    {
	pid_t child;
	int status;
	child = fork();
	if (child==0) {
		char *argv[] = {"mkdir", "-p", "/home/gery/hasil", NULL};
    	execv("/bin/mkdir", argv);
	}
	else
	{
		while((wait(&status)) > 0);
		moveFile2();
	}
    }

## Soal 2
Untuk soal nomor 2, secara garis besar kita diminta untuk membuat online judge sederhana

### Poin A
Ketika server dan client saling terhubung, akan terdapat dua pilihan masuk yaitu register atau login. Username dan password dari client akan disimpan dalam file user.txt dengan format username:password. Dimana kondisi username itu harus unique yaitu tidak boleh berbeda. Kemudian password minimal panjang 6 dengan kombinasi huruf besar, kecil, dan angka.
    
    `
    if(check==0){
        send(new_socket, "Please select: (type)\n 1. Login\n 2. Register\n", 100, 0); read(new_socket, serv_buffer, 1024);
        send(new_socket, "Username: ", 100, 0); read(new_socket, uname, 1024);
        if(strcmp(serv_buffer, "Register") == 0){
            FILE *f;
            f = fopen("users.txt", "a+");
            if(check_uname(uname, f)){
                send(new_socket, "Password: ", 100, 0);read(new_socket, password, 1024);
                if(check_pass(password)){
                    printf("Username %s\n", uname);
                    printf("Password %s\n", password);
                    fprintf(f, "%s:%s\n", uname, password);fclose(f);
                }else{
                    send(new_socket, "Password invalid", 100, 0);
                }
            }else{
                send(new_socket, "Please input another username", 100, 0);
                close(new_socket);
                exit;
            }
        }else if(strcmp(serv_buffer, "Login")==0){
            login(new_socket, uname);
        }
        check=1;
    }
    `

Ketika client memilih command register akan diminta input untuk username dan password baru yang sebelumnya akan dicek apakah sesuai ketentuan atau tidak.
    
    `
    int check_uname(char username[], FILE *fp){
    	char buff[200];
    	while(fscanf(fp, "%s", buff)!= EOF){
        	char *tok = strtok(buff, ":");
        	if (strcmp(username, tok)){
            	return 1;
        	}
    	}
    	return -1;
	}

	int check_pass(char password[]){
    	int upper = 0, lower = 0;
    	if (strlen(password) > 6){
        	for (int i = 0; i < strlen(password) + 1; i++) {
            	if (isupper(password[i])) 
                	upper++;
            	else if(islower(password[i])) 
                	lower++;
        	}
        	if(upper!=0 && lower!=0) 
            	return 1;
        	else 
            	return 0; 
    	}else 
        	return 0;
	}
    `

Selanjutnya jika client memilih command Login akan dicek melalui fungsi login dalam users.txt
    
    `
    void login(int new_socket, char username[]){
    	char login_pass[100], buffer[100], cmpr[150];
    	send(new_socket, "Password:", 100, 0); read(new_socket, login_pass, 1024);

	    FILE *fp = fopen("/home/firman/modul3/server/users.txt", "a+");
    	snprintf(cmpr, 201, "%s:%s", username, login_pass);
    	while (fscanf(fp, "%s", buffer) != EOF){
        	if(strcmp(cmpr, buffer)==0){
            	printf("Login success!\n");
            	fclose(fp);
            	return;
        	}
    	}
    	send(new_socket, "Failed login", 100, 0);
    	fclose(fp);
	}
    `

## Poin B
Membuat database problems.tsv yang berisi problem dan authornya
    
    `
    FILE *ftsv = fopen("problems.tsv", "a+");
    send(fd, "Judul problem: ", 100, 0); read(fd, judul, 1024);
    if(problemIsValid(judul)){
        send(fd, "re_inp", 200,0);
        send(fd, "Judul telah tersedia, silahkan input judul lain: ", 150,0);
        sleep(1);
        add(fd, username);
    }
    `

## Poin C
Client yang telah login dapat memilih berbagai command, salah satunya yaitu add. Add akan menambahkan judul problem baru, deskripsi, input, dan output.
    
    `
    if(problemIsValid(judul)){
    	send(fd, "re_inp", 200,0);
    	send(fd, "Judul telah tersedia, silahkan input judul lain: ", 150,0);
    	sleep(1);
    	add(fd, username);
    }

    send(fd, "Isi deskripsi (file.txt): ", 100, 0); read(fd, deskripsi, 1024);
    send(fd, "Isi input problem (file.txt): ", 100, 0); read(fd, input, 1024);
    send(fd, "Isi output problem (file.txt): ", 100, 0); read(fd, output, 1024);

    fprintf(ftsv, "%s\t%s\n", judul, username);
    fclose(ftsv);
    mkdir(judul, 0777);
    chdir(judul);
    strcat(deskripsi, "description.txt");
    strcat(input, "input.txt");
    strcat(output, "output.txt");
    `

Ketika menginput judul, akan dicek terlebih dahulu agar tidak ada yang sama
    
    `
    int check_problem(char title[]){
    	FILE *ftsv = fopen("problems.tsv", "r");
    	char buff[200];
    	while(fgets(buff, 200, ftsv)){
        	buff[strcspn(buff, "\n")] = 0;
        	char *tok = strtok(buff, "\t");
        	if(strcmp(title, tok) == 0) 
            	return 1;
    	}
    	fclose(ftsv);
    	return 0;
	}
    `

## Poin D
Menampilkan semua judul problem dengan command sebelumnya
    
    `
	void see(int fd, char username[]){
    	char cwd[200];
    	if(getcwd(cwd, sizeof(cwd))!=NULL){
        	printf("Now at %s\n", cwd);
    	}

    	FILE *ftsv = fopen("problems.tsv", "a+");
    	char buff[1001], c;
    	c = fgetc(ftsv);
    	while(c != EOF){
        	if(c == '\t'){
            	strcat(buff, " by ");
        	}
        	else{
            	strncat(buff, &c, 1);
        	}
        	c = fgetc(ftsv);
    	}
    	send(fd, buff, 1001, 0);
	}
    `

## Poin G
Server dapat menangani multiple connection, yaitu jika ada dua client yang ingin terhubung, maka harus menunggu client satunya selesai terlebih dahulu.
    
    `
	pthread_t thr;
            int *ptr_client = malloc(sizeof(int));
            *ptr_client = new_socket;
            pthread_create(&thr, NULL, &connectClient, ptr_client);
            cnt++;

    `

## Kendala
Masih belum selesai mengerjakan poin E-F, kemudian pada poin C belum bisa membuat file untuk deskripsi, input, dan output.

## Soal 3
### Poin A
Untuk poin A, kita disuruh untuk melakukan pengkategorian file secara rekursif dan setiap pengkategorian dilakukan dengan menggunakan thread. Maka, untuk kodingannya dapat dilakukan dengan cara seperti berikut ini.

	void listFilesRecursively(char *basePath, int idx)
	{
		char path[100] = "";
		struct dirent *dp;
		DIR *dir = opendir(basePath);

		if (!dir)
			return;

		while ((dp = readdir(dir)) != NULL)
		{
			if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)  
			{
				char temp[500] = "";
				strcpy(path, basePath);
				strcat(path, "/");
				strcat(path, dp->d_name);

				strcat(temp, basePath);
				if(dp->d_type == DT_DIR){
					// printf("dir: %s\n", dp->d_name);
				}else{
					pthread_t thread;
					strcat(temp, " ");
					strcat(temp, dp->d_name);
					pthread_create(&thread, NULL, categorizeEachFiles, (void*) temp);
					pthread_join(thread, NULL);
				}

				// printf("%s\n", temp);

				// // Construct new path from our base path

				listFilesRecursively(path, idx);
			}
		}

		closedir(dir);
	}

Untuk hasilnya akan terlihat seperti ini.

![image1](./soal3/screenshot/1.jpg)

### Poin B
Untuk poin B, kita akan membuat dua buah direktori, yaitu direktori Hidden dan Unknown. Untuk direktori Hidden, kita akan menyimpan file yang tidak ditampilkan pada directory tapi pasti ada, lalu untuk direktori Unknown dibuat untuk menyimpan file yang tidak memiliki ekstensi. Untuk kodingannya sendiri dapat dilihat dibawah ini.

	void* categorizeEachFiles(void* arg){
        char ext[100] = "";
        char path[100] = "";
        char namaFile[100] = "";
        char *temp = (char*)arg;
        // printf("%s\n", temp);
        char *temp1, *temp2, *temp3, *temp4;

        int cnt = 0;
        for(temp2 = strtok_r(temp, " ", &temp1); temp2 != NULL; temp2 = strtok_r(NULL, " ", &temp1)){
            if(cnt == 0){
                strcpy(path, temp2);
            }else if(cnt == 1){
                strcpy(namaFile, temp2);
            }
            cnt++;
        }


		cnt = 0;

		for(temp4 = strtok_r(namaFile, ".", &temp3); temp4 != NULL; temp4 = strtok_r(NULL, ".", &temp3)){
			if(cnt == 1){
				strcat(ext, temp4);
			}else if(cnt > 1){
				strcat(ext, ".");
				strcat(ext, temp4);
			}
			cnt++;
		}

		printf("%s\n", namaFile);

		if(cnt == 1 && namaFile[0] == '.'){//cek jika file hidden
			mkdir("/home/adioz/shift3/hartakarun/Hidden", 0777);
			char oldname[256] = "", newname[256] = "";
			//oldname
			strcpy(oldname, path);
			strcat(oldname, "/");
			strcat(oldname, namaFile);
			//newname
			strcat(newname, "/home/adioz/shift3/hartakarun/Hidden/");
			strcat(newname, namaFile);

			if(rename(oldname, newname) != 0){

			}else{
				// printf("%s -> %s\n", oldname, newname);
			}
		}else if(cnt == 1 && namaFile[0] != '.'){//cek jika file unknown
			mkdir("/home/adioz/shift3/hartakarun/Unknown", 0777);
			char oldname[256] = "", newname[256] = "";
			strcat(oldname, path);
			strcat(oldname, "/");
			strcat(oldname, namaFile);
			//newname
			strcat(newname, "/home/adioz/shift3/hartakarun/Unknown/");
			strcat(newname, namaFile);
			
			// rename
			if(rename(oldname, newname) != 0){

			}else{
				// printf("%s -> %s\n", oldname, newname);
			}
		}else{//cek jika file berekstensi
			char oldname[256] = "", newname[256] = "";
			char directory[256] = "/home/adioz/shift3/hartakarun/";

			strcat(namaFile, ".");
			strcat(namaFile, ext);
			strcat(directory, toLower(ext));
			mkdir(directory, 0777);

			//oldname 
			strcat(oldname, path);
			strcat(oldname, "/");
			strcat(oldname, namaFile);
			//newname
			strcat(newname, directory);
			strcat(newname, "/");
			strcat(newname, namaFile);

			if(rename(oldname, newname) != 0){

			}else{
				// printf("%s -> %s\n", oldname, newname);
			}
		}
	}

Untuk direktori Hidden dan Unknown akan terlihat seperti ini.

![image2](./soal3/screenshot/2.png)

Lalu untuk isi dari direktori Hidden akan terlihat seperti ini.

![image3](./soal3/screenshot/3.png)

Dan untuk isi dari direktori Unknown akan terlihat seperti ini.

![image4](./soal3/screenshot/4.png)

### Poin C
Untuk poin C sendiri dapat dilihat pada poin A terdapat deklarasi dan penggunaan thread untuk tiap pengkategorian.
### Poin D (Belum selesai)
Untuk poin D, kita dapat membuat sebuah kodingan server untuk menerima file dan client untuk mengirim file seperti kodingan dibawah.

Server

	void writeFile(int socketFd) { 
		int n;
		FILE *fp;
		char *fName = "hartakarun.zip";
		char buffer[SIZE];

		fp = fopen(fName, "w");
		while (1) {
			n = recv(socketFd, buffer, SIZE, 0);
			if (n <= 0) {
				break;
				return;
			}
			fprintf(fp, "%s", buffer);
			bzero(buffer, SIZE);
		}
		return;
	}

	int main() {
		char *ip = "127.0.0.1";
		int port = 8080;
		int e;

		int socketFd, new_sock;
		struct sockaddr_in serverAdd, new_addr;
		socklen_t addr_size;
		char buffer[SIZE];

		socketFd = socket(AF_INET, SOCK_STREAM, 0);
		if (socketFd < 0) {
			perror("~Error in socket");
			exit(1);
		}
		printf("~Server socket created successfully.\n");

		serverAdd.sin_family = AF_INET;
		serverAdd.sin_port = port;
		serverAdd.sin_addr.s_addr = inet_addr(ip);

		e = bind(socketFd, (struct sockaddr *)&serverAdd, sizeof(serverAdd));
		if (e < 0) {
			perror("~Error in bind");
			exit(1);
		}
		printf("~Binding successfull.\n");

		if (listen(socketFd, 10) == 0) {
			printf("~Listening....\n");
		}
		else{
			perror("~Error in listening");
			exit(1);
		}

		addr_size = sizeof(new_addr);
		new_sock = accept(socketFd, (struct sockaddr *)&new_addr, &addr_size);
		writeFile(new_sock);
		printf("~Data written in the file successfully.\n");

		return 0;
	}

Client
	
	void sFile(FILE *fp, int socketFd) {
		int n;
		char data[SIZE] = {0};

		while (fgets(data, SIZE, fp) != NULL) {
			if (send(socketFd, data, sizeof(data), 0) == -1) {
				perror("~Error in sending file.");
				exit(1);
			}
			bzero(data, SIZE);
		}
	}

	int main() {
		char *ip = "127.0.0.1";
		int port = 8080;
		int e;

		int socketFd;
		struct sockaddr_in serverAdd;
		FILE *fp;
		char *fName = "hartakarun.zip";

		socketFd = socket(AF_INET, SOCK_STREAM, 0);
		if (socketFd < 0) {
			perror("~Error in socket");
			exit(1);
		}
		printf("~Server socket created successfully.\n");

		serverAdd.sin_port = port;
		serverAdd.sin_addr.s_addr = inet_addr(ip);
		serverAdd.sin_family = AF_INET;


		e = connect(socketFd, (struct sockaddr *)&serverAdd, sizeof(serverAdd));
		if (e == -1) {
			perror("~Error in socket");
			exit(1);
		}
		printf("~Connected to Server.\n");

		char comm[100];
		scanf("%s", comm);
		if (strcmp(comm, "send hartakarun.zip") == 0) {
			// scanf("%s", fName);

			fp = fopen(fName, "r");
			if (fp == NULL) {
				perror("~Error in reading file.");
				exit(1);
			}

			sFile(fp, socketFd);
			printf("~File data sent successfully.\n");

			printf("~Closing the connection.\n");
			close(socketFd);
		}
		return 0;
	}

### Poin E (Belum selesai)
Ketika dikirim command, maka kita dapat menulis command ```send hartakarun.zip``` dan dari servernya akan menerima file hartakarun.zip dari clientnya. Untuk kodingannya dapat dilihat dibawah.

	char comm[100];
	scanf("%s", comm);
	if (strcmp(comm, "send hartakarun.zip") == 0) {
		// scanf("%s", fName);

		fp = fopen(fName, "r");
		if (fp == NULL) {
			perror("~Error in reading file.");
			exit(1);
		}

		sFile(fp, socketFd);
		printf("~File data sent successfully.\n");

		printf("~Closing the connection.\n");
		close(socketFd);
	}

### Kendala Selama Pengerjaan
Untuk kendalanya sendiri cukup banyak pada soal kali ini, yaitu pada bagian pengkategorian masih ada file yang mengandung spasi belum bisa dipindahkan ke direktori masing-masing, lalu untuk pengiriman secara server-client untuk file zip masih belum bisa terimplementasikan, dan selama pengerjaan masih bingung untuk implementasi dari thread itu sendiri.
