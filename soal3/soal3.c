#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>

int idx = 0;

char* toLower(char* str){
    int i;
    for(i = 0; str[i]; i++){
        str[i] = tolower(str[i]);
    }
    return str;
}

void* categorizeEachFiles(void* arg){
    char ext[100] = "";
    char path[100] = "";
    char namaFile[100] = "";
    char *temp = (char*)arg;
    // printf("%s\n", temp);
    char *temp1, *temp2, *temp3, *temp4;

    int cnt = 0;
    for(temp2 = strtok_r(temp, " ", &temp1); temp2 != NULL; temp2 = strtok_r(NULL, " ", &temp1)){
        if(cnt == 0){
            strcpy(path, temp2);
        }else if(cnt == 1){
            strcpy(namaFile, temp2);
        }
        cnt++;
    }


    cnt = 0;

    for(temp4 = strtok_r(namaFile, ".", &temp3); temp4 != NULL; temp4 = strtok_r(NULL, ".", &temp3)){
        if(cnt == 1){
            strcat(ext, temp4);
        }else if(cnt > 1){
            strcat(ext, ".");
            strcat(ext, temp4);
        }
        cnt++;
    }

    printf("%s\n", namaFile);

    if(cnt == 1 && namaFile[0] == '.'){//cek jika file hidden
        mkdir("/home/adioz/shift3/hartakarun/Hidden", 0777);
        char oldname[256] = "", newname[256] = "";
        //oldname
        strcpy(oldname, path);
        strcat(oldname, "/");
        strcat(oldname, namaFile);
        //newname
        strcat(newname, "/home/adioz/shift3/hartakarun/Hidden/");
        strcat(newname, namaFile);

        if(rename(oldname, newname) != 0){

        }else{
            // printf("%s -> %s\n", oldname, newname);
        }
    }else if(cnt == 1 && namaFile[0] != '.'){//cek jika file unknown
        mkdir("/home/adioz/shift3/hartakarun/Unknown", 0777);
        char oldname[256] = "", newname[256] = "";
        strcat(oldname, path);
        strcat(oldname, "/");
        strcat(oldname, namaFile);
        //newname
        strcat(newname, "/home/adioz/shift3/hartakarun/Unknown/");
        strcat(newname, namaFile);
        
        // rename
        if(rename(oldname, newname) != 0){

        }else{
            // printf("%s -> %s\n", oldname, newname);
        }
    }else{//cek jika file berekstensi
        char oldname[256] = "", newname[256] = "";
        char directory[256] = "/home/adioz/shift3/hartakarun/";

        strcat(namaFile, ".");
        strcat(namaFile, ext);
        strcat(directory, toLower(ext));
        mkdir(directory, 0777);

        //oldname 
        strcat(oldname, path);
        strcat(oldname, "/");
        strcat(oldname, namaFile);
        //newname
        strcat(newname, directory);
        strcat(newname, "/");
        strcat(newname, namaFile);

        if(rename(oldname, newname) != 0){

        }else{
            // printf("%s -> %s\n", oldname, newname);
        }
    }
}

void listFilesRecursively(char *basePath, int idx)
{
    char path[100] = "";
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)  
        {
            char temp[500] = "";
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            strcat(temp, basePath);
            if(dp->d_type == DT_DIR){
                // printf("dir: %s\n", dp->d_name);
            }else{
                pthread_t thread;
                strcat(temp, " ");
                strcat(temp, dp->d_name);
                pthread_create(&thread, NULL, categorizeEachFiles, (void*) temp);
                pthread_join(thread, NULL);
            }

            // printf("%s\n", temp);

            // // Construct new path from our base path

            listFilesRecursively(path, idx);
        }
    }

    closedir(dir);
}

void* categorizeFiles(){
    char* path = "/home/adioz/shift3/hartakarun";
    listFilesRecursively(path, idx);
}

void zipFiles(){
    pid_t child;
    child = fork();
    if(child == 0){
        char* argv[] = {"zip", "-r", "hartakarun.zip", "shift3/hartakarun", NULL};
        execv("/bin/zip", argv);
    }
}

int
main(){
    mkdir("/home/adioz/shift3", 0777);
    categorizeFiles();
    zipFiles();
}