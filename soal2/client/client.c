#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#define PORT 8080

int clientcrt(struct sockaddr_in serv_addr);

int main(){
    int valread, sock;
    struct sockaddr_in serv_addr;
    sock = clientcrt(serv_addr);

    while(1){
        fflush(stdin);
        char buffer[100]={0}, server_buffer[1024]={0};
        read(sock, server_buffer, 1024);

        if(strcmp(server_buffer, "multi-input")==0){
            printf("%s\n", server_buffer); memset(server_buffer, 0, strlen(server_buffer));
            read(sock, server_buffer, 1024);

            printf("%s\n", server_buffer); memset(server_buffer, 0, strlen(server_buffer));
            read(sock, server_buffer, 1024);
        }
        printf("%s\n", server_buffer); fgets(buffer, 200, stdin);
        buffer[strcspn(buffer, "\n")] = 0;
        send(sock, buffer, 100, 0);
    }
    return 0;
}

int clientcrt(struct sockaddr_in serv_addr){
    int sock;
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("\n Socket creation error \n");
        return -1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0){
        printf("\n Invalid address / Addres not supported \n");
        return -1;
    }
    if(connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr))<0){
        printf("\n Connection Failed\n");
        return -1;
    }
    return sock;
}