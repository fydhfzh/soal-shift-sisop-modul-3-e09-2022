#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <netinet/in.h>
#define PORT 8080

int check_uname(char username[], FILE *fp){
    char buff[200];
    while(fscanf(fp, "%s", buff)!= EOF){
        char *tok = strtok(buff, ":");
        if (strcmp(username, tok)){
            return 1;
        }
    }
    return -1;
}

int check_pass(char password[]){
    int upper = 0, lower = 0;
    if (strlen(password) > 6){
        for (int i = 0; i < strlen(password) + 1; i++) {
            if (isupper(password[i])) 
                upper++;
            else if(islower(password[i])) 
                lower++;
        }
        if(upper!=0 && lower!=0) 
            return 1;
        else 
            return 0; 
    }else 
        return 0;
}


void see(int fd, char username[]){
    char cwd[200];
    if(getcwd(cwd, sizeof(cwd))!=NULL){
        printf("Now at %s\n", cwd);
    }

    FILE *ftsv = fopen("problems.tsv", "a+");
    char buff[1001], c;
    c = fgetc(ftsv);
    while(c != EOF){
        if(c == '\t'){
            strcat(buff, " by ");
        }
        else{
            strncat(buff, &c, 1);
        }
        c = fgetc(ftsv);
    }
    send(fd, buff, 1001, 0);
}

int check_problem(char title[]){
    FILE *ftsv = fopen("problems.tsv", "r");
    char buff[200];
    while(fgets(buff, 200, ftsv)){
        buff[strcspn(buff, "\n")] = 0;
        char *tok = strtok(buff, "\t");
        if(strcmp(title, tok) == 0) 
            return 1;
    }
    fclose(ftsv);
    return 0;
}

void add(int fd, char username[]){
    char judul[100], deskripsi[100], input[100], output[100], ch;
    FILE *ftsv = fopen("problems.tsv", "a+");
    send(fd, "Judul problem: ", 100, 0); read(fd, judul, 1024);
    if(check_problem(judul)){
        send(fd, "re_inp", 200,0);
        send(fd, "Judul telah tersedia, silahkan input judul lain: ", 150,0);
        sleep(1);
        add(fd, username);
    }
    send(fd, "Isi deskripsi (file.txt): ", 100, 0); read(fd, deskripsi, 1024);
    send(fd, "Isi input problem (file.txt): ", 100, 0); read(fd, input, 1024);
    send(fd, "Isi output problem (file.txt): ", 100, 0); read(fd, output, 1024);

    fprintf(ftsv, "%s\t%s\n", judul, username);
    fclose(ftsv);
    mkdir(judul, 0777);
    chdir(judul);
    strcat(deskripsi, "description.txt");
    strcat(input, "input.txt");
    strcat(output, "output.txt");
}

void login(int new_socket, char username[]){
    char login_pass[100], buffer[100], cmpr[150];
    send(new_socket, "Password:", 100, 0); read(new_socket, login_pass, 1024);

    FILE *fp = fopen("/home/firman/modul3/server/users.txt", "a+");
    snprintf(cmpr, 201, "%s:%s", username, login_pass);
    while (fscanf(fp, "%s", buffer) != EOF){
        if(strcmp(cmpr, buffer)==0){
            printf("Login success!\n");
            fclose(fp);
            return;
        }
    }
    send(new_socket, "Failed login", 100, 0);
    fclose(fp);
}

void *connectClient(void *argv){
    int new_socket;
    char uname[100], cmd_buffer[1024], serv_buffer[1024], password[100];
    new_socket = *((int *)argv);
    free(argv);
    int check=0;
    
    if(check==0){
        send(new_socket, "Please select: (type)\n 1. Login\n 2. Register\n", 100, 0); read(new_socket, serv_buffer, 1024);
        send(new_socket, "Username: ", 100, 0); read(new_socket, uname, 1024);
        if(strcmp(serv_buffer, "Register") == 0){
            FILE *f;
            f = fopen("users.txt", "a+");
            if(check_uname(uname, f)){
                send(new_socket, "Password: ", 100, 0);read(new_socket, password, 1024);
                if(check_pass(password)){
                    printf("Username %s\n", uname);
                    printf("Password %s\n", password);
                    fprintf(f, "%s:%s\n", uname, password);fclose(f);
                }else{
                    send(new_socket, "Password invalid", 100, 0);
                }
            }else{
                send(new_socket, "Please input another username", 100, 0);
                close(new_socket);
                exit;
            }
        }else if(strcmp(serv_buffer, "Login")==0){
            login(new_socket, uname);
        }
        check=1;
    }

    while(check){
        char cwd[100];
        if(getcwd(cwd, sizeof(cwd))!=NULL) 
            printf("Now at %s \n", cwd);

        chdir("/home/firman/");
        send(new_socket, "Login success\n Insert the command below (type the command):\n1. add\n2. see", 200, 0); read(new_socket, cmd_buffer, 1024);
        if(strcmp(cmd_buffer, "add") == 0) 
            add(new_socket, uname);
        else if(strcmp(cmd_buffer, "see") == 0) 
            see(new_socket, uname);
        else close(new_socket);
    }
}

int createServer(struct sockaddr_in address, int addrlen){
    int server_fd, opt=1;
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if(listen(server_fd, 3)<0){
        perror("listen");
        exit(EXIT_FAILURE);
    }
    return server_fd;
}

int main(int argc, char const *argv[]){
    int cnt=0;
    struct sockaddr_in address;
    int new_socket, addrlen = sizeof(address);

    while (1){
        if(cnt==0){
            int server_fd = createServer(address, addrlen);
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0){
                perror("accept");
                exit(EXIT_FAILURE);
            }
            pthread_t thr;
            int *ptr_client = malloc(sizeof(int));
            *ptr_client = new_socket;
            pthread_create(&thr, NULL, &connectClient, ptr_client);
            cnt++;
        }
    }
    return 0;
}
